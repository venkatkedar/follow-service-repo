package com.projectx.followcontroller.controllers;

import com.projectx.followservice.model.FollowVO;

import java.util.Optional;

import com.projectx.followservice.api.IFollowService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@RestController
@RequestMapping("/v1")
@Slf4j
public class FollowController {
    private IFollowService followService;

    @Autowired
    public FollowController(IFollowService followService){
        this.followService=followService;
    } 

    @RequestMapping(value="/follow", method=RequestMethod.POST)
    public Mono<FollowVO> follow( @RequestBody FollowVO followVO
                                /*@PathVariable("followerId") long followerId,@PathVariable("followeeId") long followeeId*/) {
        log.debug("user id:"+followVO.getFollowerId()+" follows user id :"+followVO.getFolloweeId());
        return followService.follow(followVO);
    }
    

    @RequestMapping(value="/unfollow", method=RequestMethod.POST)
    public Mono<Void> unFollow( @RequestBody FollowVO followVO
                                /*@PathVariable("followerId") long followerId,@PathVariable("followeeId") long followeeId*/) {
                                    log.debug("user id:"+followVO.getFollowerId()+" unfollows user id :"+followVO.getFolloweeId());
        return followService.unfollow(followVO);
    }

    @RequestMapping(value="/follower/{followerId}/followees", method=RequestMethod.GET)
    public Flux<FollowVO> getFollowees(@PathVariable("followerId") long followerId) {
        log.debug("getting followees of user id:"+followerId);
        return followService.getFollowees(followerId);
    }

    @RequestMapping(value="/followee/{followeeId}/followers", method=RequestMethod.GET)
    public Flux<FollowVO> getFollowers(@PathVariable("followeeId") long followeeId) {
        log.debug("getting followers of user id:"+followeeId);
        return followService.getFollowers(followeeId);
    }

    @RequestMapping(value="/celebrity-followees/follower/{followerId}", method=RequestMethod.GET)
    public Flux<FollowVO> getCelebrityFollowees(@PathVariable("followerId") long followerId) {
        log.debug("getting celebrity followees of user id:"+followerId);
        return followService.getCelebrityFollowees(followerId);
    }
}
