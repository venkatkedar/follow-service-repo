package com.projectx.follow.dao.api;

import com.common.dao.api.ReactiveBaseDao;
import com.projectx.follow.dao.model.FollowEO;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IFollowDao extends ReactiveBaseDao<FollowEO>{
    Mono<FollowEO> follow(long followerId,long followeeId);
	Mono<Void> unfollow(long followerId,long followeeId);
	Flux<FollowEO> getFollowees(long followerId);
	Flux<FollowEO> getFollowers(long followeeId);
	Flux<FollowEO> getCelebrityFollowees(long followerId);
}
