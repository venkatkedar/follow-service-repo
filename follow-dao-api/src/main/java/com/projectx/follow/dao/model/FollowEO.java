package com.projectx.follow.dao.model;

import com.common.dao.entities.AbstractEO;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Document
@AllArgsConstructor
@NoArgsConstructor
public class FollowEO extends AbstractEO {
    
    private long followerId;
    private long followeeId;
    
}
