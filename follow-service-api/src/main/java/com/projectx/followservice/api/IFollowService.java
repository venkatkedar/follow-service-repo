package com.projectx.followservice.api;


import com.projectx.followservice.model.FollowVO;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IFollowService {
    Mono<FollowVO> follow(FollowVO followVO);
	Mono<Void> unfollow(FollowVO followVO);
	Flux<FollowVO> getFollowees(long followerId);
	Flux<FollowVO> getCelebrityFollowees(long followerId);
	Flux<FollowVO> getFollowers(long followeeId);
}
