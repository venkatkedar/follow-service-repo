package com.projectx.followservice.model;

import com.common.service.valueobjects.AbstractVO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FollowVO extends AbstractVO{
    private long followerId;
    private long followeeId;
}
