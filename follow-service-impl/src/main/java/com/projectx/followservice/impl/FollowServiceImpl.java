package com.projectx.followservice.impl;

import java.util.Date;

import com.common.dao.exceptions.EntityNotFoundException;
import com.common.service.exceptions.NotFoundException;
import com.common.service.exceptions.ServiceException;

import com.common.utils.converter.Converter;
import com.projectx.followservice.api.IFollowService;
import com.projectx.followservice.model.FollowVO;
import com.projectx.follow.dao.api.IFollowDao;
import com.projectx.follow.dao.model.FollowEO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@NoArgsConstructor
@Slf4j
@Service
public class FollowServiceImpl implements IFollowService {

    private IFollowDao followdao;

    @Autowired
    public FollowServiceImpl(IFollowDao followdao){
        this.followdao=followdao;       
    }

    @Override
    public Mono<FollowVO> follow(FollowVO followVO){
        log.info("user id:"+followVO.getFollowerId()+" follows user id :"+followVO.getFolloweeId());
        Date d=new Date();        
        followVO.setLastUpdatedTime(d);
        FollowEO fEO=Converter.convertUToV(followVO, FollowEO.class);
        Mono<FollowEO> followEO= followdao.add(fEO);
        
        return followEO.map(f->Converter.convertUToV(f, FollowVO.class));
    }

    @Override
    public Mono<Void> unfollow(FollowVO followVO){
        log.info("user id:"+followVO.getFollowerId()+" unfollows user id :"+followVO.getFolloweeId());        
        FollowEO fEO=Converter.convertUToV(followVO, FollowEO.class);
        return followdao.delete(fEO);
    }

    @Override
    public Flux<FollowVO> getFollowees(long followerId){
        log.info("getting followees of user id:"+followerId);
        Flux<FollowEO> fEO= followdao.getFollowees(followerId);
        return fEO.map(f->Converter.convertUToV(f, FollowVO.class));
    }

    @Override
    public Flux<FollowVO> getFollowers(long followeeId){
        log.info("getting followers of user id:"+followeeId);
        Flux<FollowEO> fEO= followdao.getFollowers(followeeId);
        return fEO.map(f->Converter.convertUToV(f, FollowVO.class));
    }

    @Override
    public Flux<FollowVO> getCelebrityFollowees(long followerId){
        log.info("getting celebrity followees of user id:"+followerId);
        Flux<FollowEO> fEO= followdao.getCelebrityFollowees(followerId);
        return fEO.map(f->Converter.convertUToV(f, FollowVO.class));
    }

}
