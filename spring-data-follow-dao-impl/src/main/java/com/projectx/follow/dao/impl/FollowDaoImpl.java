package com.projectx.follow.dao.impl;

import java.util.List;

import com.common.dao.spring.impl.AbstractSpringDataReactiveDaoImpl;
import com.common.dao.spring.impl.SpringDataBaseDaoImpl;
import com.projectx.follow.dao.api.IFollowDao;
import com.projectx.follow.dao.model.FollowEO;
import com.projectx.follow.dao.repository.FollowEOReactiveRepository;
import com.projectx.follow.dao.repository.FollowEORepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Data
@Repository
@Slf4j
public class FollowDaoImpl extends AbstractSpringDataReactiveDaoImpl<FollowEO, FollowEOReactiveRepository>
        implements IFollowDao {
    private FollowEOReactiveRepository repository;

    @Autowired
    public FollowDaoImpl(FollowEOReactiveRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public Mono<FollowEO> follow(long followerId, long followeeId) {
        FollowEO eo=new FollowEO(followerId,followeeId);
        return repository.save(eo);
    }

    @Override
    public Mono<Void> unfollow(long followerId, long followeeId) {
        FollowEO eo=new FollowEO(followerId,followeeId);
        return repository.delete(eo);
    }

    @Override
    public Flux<FollowEO> getFollowees(long followerId) {
        return repository.findByFollowerId(followerId);
    }

    @Override
    public Flux<FollowEO> getFollowers(long followeeId) {
        return repository.findByFolloweeId(followeeId);
    }

    @Override
    public Flux<FollowEO> getCelebrityFollowees(long followerId) {
        return repository.findCelebrityFollowees(followerId);
    }

    
}
