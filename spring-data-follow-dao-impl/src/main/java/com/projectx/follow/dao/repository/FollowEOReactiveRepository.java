package com.projectx.follow.dao.repository;

import com.common.dao.spring.api.AbstractEOReactiveRepository;
import com.projectx.follow.dao.model.FollowEO;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import reactor.core.publisher.Flux;

@Repository
public interface FollowEOReactiveRepository extends AbstractEOReactiveRepository<FollowEO>{
	@Query("delete from follow where followerId=:fId and followeeId=:flweeId")
	void unfollow(@Param("fId") long followerId, @Param("flweeId") long followeeId);
	
	@Query("select followerId,followeeId,lastUpdatedTime from follow f , celebrity c where f.followeeId==c.id and f.followerId=:fId")
	Flux<FollowEO> findCelebrityFollowees(@Param("fId")long followerId);
    
    //@Query("select followerId,followeeId,lastUpdatedTime from follow f where f.followerId=:fId")
	Flux<FollowEO> findByFollowerId(@Param("fId")long followerId);
    
    //@Query("select followerId,followeeId,lastUpdatedTime from follow f where f.followeeId=:flweeId")
	Flux<FollowEO> findByFolloweeId(@Param("flweeId")long followeeId);
}
