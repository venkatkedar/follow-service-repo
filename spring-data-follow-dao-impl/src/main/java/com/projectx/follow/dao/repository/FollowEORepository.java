package com.projectx.follow.dao.repository;

import com.common.dao.spring.api.AbstractEORepository;
import com.projectx.follow.dao.model.FollowEO;

public interface FollowEORepository extends AbstractEORepository<FollowEO>{
    
}
